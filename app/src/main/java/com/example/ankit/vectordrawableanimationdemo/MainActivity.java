package com.example.ankit.vectordrawableanimationdemo;

import android.graphics.drawable.AnimatedVectorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private boolean isBack = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ImageView ivIcon = (ImageView) findViewById(R.id.iv_icon);

        if(ivIcon != null) {
            ivIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isBack) {
                        // transform from back icon to search icon
                        AnimatedVectorDrawable backToSearch = (AnimatedVectorDrawable) ContextCompat
                                .getDrawable(MainActivity.this, R.drawable.avd_back_to_search);
                        ivIcon.setImageDrawable(backToSearch);
                        // clear the background else the touch ripple moves with the translation which looks bad
                        ivIcon.setBackground(null);
                        backToSearch.start();
                        isBack = false;
                    } else {
                        // transform from search icon to back icon
                        AnimatedVectorDrawable searchToBack = (AnimatedVectorDrawable) ContextCompat
                                .getDrawable(MainActivity.this, R.drawable.avd_search_to_back);
                        ivIcon.setImageDrawable(searchToBack);
                        searchToBack.start();
                        // for some reason the animation doesn't always finish (leaving a part arrow!?) so after
                        // the animation set a static drawable. Also animation callbacks weren't added until API23
                        // so using post delayed :(
                        // TODO fix properly!!
                        ivIcon.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                ivIcon.setImageDrawable(ContextCompat.getDrawable(MainActivity.this,
                                        R.drawable.ic_arrow_back_padded));
                            }
                        }, 600L);
                        isBack = true;
                    }
                }
            });
        }
    }
}
